import cookieParser from 'cookie-parser';
import cors from 'cors';
import createError, { HttpError } from 'http-errors';
import express, { NextFunction, Request, Response } from 'express';
import logger from 'morgan';
import path from 'path';

import { gateway } from './braintree-gateway';

import checkout from './routes/checkout';
import clientToken from './routes/client-token';
import customer from './routes/customer';
import subscription from './routes/subscription';

// port is now available to the Node.js runtime
// as if it were an environment variable
const port = process.env.SERVER_PORT;

const app = express();

// TODO: Use *.avery.com
const corsOptions = {
  origin: 'localhost'
};

// Configure Express to use EJS (can use whatever templating system)
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// TODO: Implement a logger system for easier tracking/debugging
app.use(logger('dev'));
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// define a route handler for the default home page
app.get('/', (_req: Request, res: Response) => {
  // render the index template
  res.render('index');
});

app.use('/client_token', clientToken);
app.use('/checkout', checkout);
app.use('/customer', customer);
app.use('/subscription', subscription);

// catch 404 and forward to error handler
app.use((_req: Request, _res: Response, next: NextFunction) => {
  next(createError(404));
});

// error handler
app.use((err: HttpError, req: Request, res: Response, _next: NextFunction) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

// start the express server
app.listen(port, () => {
  // tslint:disable-next-line:no-console
  console.log(`server started at http://localhost:${port} 🚀`);
});
