import express, { NextFunction, Request, Response } from 'express';

import { gateway } from '../braintree-gateway';

const router = express.Router();

router.post('/', async (req: Request, res: Response, _next: NextFunction) => {
  // Use the payment method nonce here
  const nonceFromClient = req.body.paymentMethodNonce;

  // Create a new transaction for $10
  try {
    const newTransaction = await gateway.transaction.sale({
      amount: '10.00',
      paymentMethodNonce: nonceFromClient,
      options: {
        submitForSettlement: true
      }
    });

    res.send(newTransaction);
  } catch (err) {
    console.error(err);
    res.status(500).send(err);
  }
});

export default router;
