import express, { NextFunction, Request, Response } from 'express';
import { Plan, Subscription } from 'braintree';
import axios, { AxiosResponse } from 'axios';

import { gateway } from '../braintree-gateway';

interface PlanRes {
  plans: Plan[];
  success: boolean;
}

interface TaxRes {
  totalRate: number;
  rates: Rate[];
}

interface Rate {
  rate: number;
  name: string;
  type: string;
}

const router = express.Router();

// TODO: Utilize search below for subscription renewal. Can use cron to automate JS file to update tax
// Part 1 of 2 to update taxes (export to separate files)
router.get('/', async (_req: Request, res: Response, _next: NextFunction) => {
  try {
    const subscriptions: Subscription[] = [];
    const stream = gateway.subscription.search((search: any) => {
      search.planId().is('avery-gold', 'avery-plat');
    });
    stream.on('data', (data: any) => {
      subscriptions.push(data);
    });

    stream.on('end', () => {
      res.send({ subscriptions });
    });
  } catch (err) {
    console.error(err);
    res.status(500).send(err);
  }
});

router.get(
  '/plans',
  async (_req: Request, res: Response, _next: NextFunction) => {
    try {
      const plans = ((await gateway.plan.all()) as unknown) as PlanRes;
      // TODO: Find better ways to hide "deleted" plans
      plans.plans = plans.plans.filter(
        (plan: Plan) => plan.id !== 'avery-delete'
      );
      res.send(plans);
    } catch (err) {
      console.error(err);
      res.status(500).send(err);
    }
  }
);

router.get(
  '/get-tax',
  async (req: Request, res: Response, _next: NextFunction) => {
    try {
      // Get tax info from AvaTax
      const { data }: AxiosResponse<TaxRes> = await axios.get(
        'https://sandbox-rest.avatax.com/api/v2/taxrates/bypostalcode',
        {
          headers: {
            Authorization:
              'Basic amRpZXBAYXZlcnkuY29tOlVkRnpNcUJuZ1hmdzR1ZEFrQEZN'
          },
          params: {
            country: 'US',
            postalCode: req.query.postalCode
          }
        }
      );

      res.send(data);
    } catch (err) {
      console.error(err);
      res.status(500).send(err);
    }
  }
);

router.post(
  '/create',
  async (req: Request, res: Response, _next: NextFunction) => {
    try {
      const { plans } = ((await gateway.plan.all()) as unknown) as PlanRes;
      const plan = plans.find((p: Plan) => p.id === req.body.planId);

      // Get tax info from AvaTax
      const { data }: AxiosResponse<TaxRes> = await axios.get(
        'https://sandbox-rest.avatax.com/api/v2/taxrates/bypostalcode',
        {
          headers: {
            Authorization:
              'Basic amRpZXBAYXZlcnkuY29tOlVkRnpNcUJuZ1hmdzR1ZEFrQEZN'
          },
          params: {
            country: 'US',
            postalCode: req.body.postalCode
          }
        }
      );
      const tax = Math.round(Number(plan.price) * data.totalRate * 100) / 100;

      const result = await gateway.subscription.create({
        paymentMethodToken: req.body.paymentMethodToken,
        planId: req.body.planId,
        addOns: {
          add: [
            {
              inheritedFromId: 'subscription-tax',
              amount: tax.toString()
            }
          ]
        }
      });

      res.send(result);
    } catch (err) {
      console.error(err);
      res.status(500).send(err);
    }
  }
);

// NOTE: If upgrade/downgrading plans will need to update every field of the
// plan that will change (add-ons/discounts, billing dates, price, etc.)
router.patch(
  '/modify',
  async (req: Request, res: Response, _next: NextFunction) => {
    try {
      const {
        plans
      }: PlanRes = ((await gateway.plan.all()) as unknown) as PlanRes;
      const plan = plans.find((p: Plan) => p.id === req.body.planId);

      if (plan) {
        const data = await gateway.subscription.update(
          req.body.subscriptionId,
          {
            paymentMethodToken: req.body.paymentMethodToken,
            price: plan.price,
            planId: plan.id
          }
        );
        res.send(data);
      } else {
        res.status(404).send('plan does not exist');
      }
    } catch (err) {
      console.error(err);
      res.status(500).send(err);
    }
  }
);

// Part 2 of 2 to update taxes (export to separate file)
router.patch(
  '/update-tax',
  async (req: Request, res: Response, _next: NextFunction) => {
    try {
      const {
        plans
      }: PlanRes = ((await gateway.plan.all()) as unknown) as PlanRes;
      const plan = plans.find((p: Plan) => p.id === req.body.planId);

      // Get tax info from AvaTax
      const { data }: AxiosResponse<TaxRes> = await axios.get(
        'https://sandbox-rest.avatax.com/api/v2/taxrates/bypostalcode',
        {
          headers: {
            Authorization:
              'Basic amRpZXBAYXZlcnkuY29tOlVkRnpNcUJuZ1hmdzR1ZEFrQEZN'
          },
          params: {
            country: 'US',
            postalCode: req.body.postalCode
          }
        }
      );

      const tax = Math.round(Number(plan.price) * data.totalRate * 100) / 100;

      const updateRes = await gateway.subscription.update(
        req.body.subscriptionId,
        {
          addOns: {
            // TODO: Add logic to determine add/update
            add: [
              {
                inheritedFromId: 'subscription-tax',
                amount: tax.toString()
              }
            ]
          },
          paymentMethodToken: req.body.paymentMethodToken,
          planId: req.body.planId
        }
      );
      console.log(updateRes);
      res.send(updateRes);
    } catch (err) {
      console.error(err);
      res.status(500).send(err);
    }
  }
);

router.delete(
  '/cancel/:id?',
  async (req: Request, res: Response, _next: NextFunction) => {
    try {
      const result = await gateway.subscription.cancel(req.params.id);

      res.send(result);
    } catch (err) {
      console.error(err);
      res.status(500).send(err);
    }
  }
);

export default router;
