import { CustomerCreateRequest } from 'braintree';
import express, { NextFunction, Request, Response } from 'express';

import { gateway } from '../braintree-gateway';

const router = express.Router();

// Find/return customer data
router.get('/', async (req: Request, res: Response, _next: NextFunction) => {
  try {
    // const customer = await gateway.customer.find(req.body.id);
    const customer = await gateway.customer.find(req.query.id);
    res.send(customer);
  } catch (err) {
    console.error(err);
    res.status(500).send(err);
  }
});

// Add a customer
router.post('/', async (req: Request, res: Response, _next: NextFunction) => {
  const customerInfo: CustomerCreateRequest = {
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    company: req.body.company,
    email: req.body.email,
    phone: req.body.phone,
    website: req.body.website
  };

  if (req.body?.paymentMethodNonce) {
    customerInfo.paymentMethodNonce = req.body.paymentMethodNonce;
  }

  try {
    const result = await gateway.customer.create(customerInfo);
    // TODO: Instead of returning customer data back to client, return just the success
    res.send(result);
  } catch (err) {
    console.error(err);
    res.status(500).send(err);
  }
});

// Update a customer
router.patch('/', async (req: Request, res: Response, _next: NextFunction) => {
  try {
    const { id, ...customerUpdates } = req.body;
    const result = await gateway.customer.update(id, customerUpdates);
    // TODO: Instead of returning customer data back to client, return just the success
    res.send(result);
  } catch (err) {
    console.error(err);
    res.status(500).send(err);
  }
});

// Delete a customer
router.delete('/', async (req: Request, res: Response, _next: NextFunction) => {
  try {
    const result = await gateway.customer.delete(req.body.id);
    res.send(result);
  } catch (err) {
    console.error(err);
    res.status(500).send(err);
  }
});

// TODO: Search for customers
// router.get('/search', async (req: Request, res: Response, _next: NextFunction) => {

// });

export default router;
