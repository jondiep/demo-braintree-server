import express, { Request, Response } from 'express';

import { gateway } from '../braintree-gateway';

const router = express.Router();

router.get('/', async (req: Request, res: Response) => {
  try {
    const clientToken = {
      customerId: ''
    };

    if (req.query?.id) {
      clientToken.customerId = req.query.id;
    }

    const data = await gateway.clientToken.generate(clientToken);

    if (!data.success) {
      throw data;
    }

    res.send({ clientToken: data.clientToken });
  } catch (err) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err.message : {};

    // render the error page
    res.status(err.status || 500);
    res.render('errors', { message: err.message });
  }
});

export default router;
