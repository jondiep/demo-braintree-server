import braintree from 'braintree';
import dotenv from 'dotenv';

// initialize configuration
dotenv.config();

export const gateway = braintree.connect({
  environment: braintree.Environment.Sandbox,
  merchantId: process.env.MERCHANT_ID,
  publicKey: process.env.PUBLIC_KEY,
  privateKey: process.env.PRIVATE_KEY
});
