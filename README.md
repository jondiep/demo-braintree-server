# Braintree Server (Local)

This is the repo to create the Braintree server that will interact with the Braintree payment server and any application that contains the Braintree client.

# Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

Here is the official Braintree guide for subscriptions:
[https://developers.braintreepayments.com/guides/overview](https://developers.braintreepayments.com/guides/overview)

### Prerequisites

Please use node v12+. It is recommended to use nvm for local node version management. [dotenv](https://www.npmjs.com/package/dotenv) is used for environment configuration. In order to configure an environment, each repo will need to have a `.env` file at the project root containing the info below.

```
# Set to production when deploying to production
NODE_ENV=development

# Node.js server configuration
SERVER_PORT=8080

# Braintree ID and keys
MERCHANT_ID=...
PUBLIC_KEY=...
PRIVATE_KEY=...
```

**Note**: The `.env` file is included in `.gitignore` so that API keys won't accidentally be pushed into repos

### Installing

A step by step series of examples that tell you how to get a development env running

```
npm install
```

## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example

```

### And coding style tests

Explain what these tests test and why

```
Give an example

```

## Deployment

To run/serve the project locally:

```
npm run dev
```

To build project:

```
npm run build
```

## Built With

- [Node.js](https://nodejs.org/en/)
- [Express](https://expressjs.com/)
- [Braintree](https://developers.braintreepayments.com/)

## Contributing

Please use [Prettier](https://prettier.io/) when making a PR
